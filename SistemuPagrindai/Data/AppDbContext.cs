﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Domain.Models;
using Microsoft.AspNetCore.Mvc.Formatters;

namespace SistemuPagrindai.Data
{
    public class AppDbContext : IdentityDbContext<AspNetUsers>
    {
        public AppDbContext(DbContextOptions<AppDbContext> options) : base(options)
        {

        }

        public DbSet<Order> Orders { get; set; }
        public DbSet<OrderItem> OrderItem { get; set; }
        public DbSet<CartItem> CartItem { get; set; }
        public DbSet<Cart> Cart { get; set; }
        public DbSet<Item> Item { get; set; }
        public DbSet<ItemRoute> ItemRoutes { get; set; }
        public DbSet<Route> Route { get; set; }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);
            builder.Entity<Order>()
                .Property(e => e.Id)
                .ValueGeneratedOnAdd();
            builder.Entity<OrderItem>()
                .Property(e => e.Id)
                .ValueGeneratedOnAdd();
            builder.Entity<Cart>()
                .Property(e => e.Id)
                .ValueGeneratedOnAdd();
            builder.Entity<Cart>()
                .HasOne(e => e.Order)
                .WithOne(a => a.Cart)
                .HasForeignKey<Order>(e => e.CartId);

            builder.Entity<CartItem>()
               .Property(e => e.Id)
               .ValueGeneratedOnAdd();

            builder.Entity<Item>()
                .Property(e => e.Id)
                .ValueGeneratedOnAdd();

            
            builder.Entity<ItemRoute>()
                .Property(e => e.Id)
                .ValueGeneratedOnAdd();
            builder.Entity<ItemRoute>()
                .HasKey(e => new { e.ItemId, e.RouteId });
            builder.Entity<ItemRoute>()
                .HasOne(e => e.Item)
                .WithMany(i => i.ItemRoutes)
                .HasForeignKey(e => e.ItemId);
            builder.Entity<ItemRoute>()
                .HasOne(e => e.Route)
                .WithMany(i => i.ItemRoutes)
                .HasForeignKey(e => e.RouteId);

            builder.Entity<Route>()
                .Property(e => e.Id)
                .ValueGeneratedOnAdd();
        }
    }
}
