﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemuPagrindai.Data
{
    public class AspNetUsers : IdentityUser
    {
        public string Iban { get; set; }
    }
}
