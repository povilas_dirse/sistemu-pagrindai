using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using SistemuPagrindai.Data;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Components.Server;
using System.Net.Http;
using SistemuPagrindai.Services;
using AutoMapper;
using Blazored.Modal;
using Blazored.Toast;
using Domain.Models;
using SistemuPagrindai.Mappings;
using NETCore.MailKit.Extensions;
using NETCore.MailKit.Infrastructure.Internal;
using NETCore.MailKit.Core;
using SistemuPagrindai.Services.EmailService;

namespace SistemuPagrindai
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddDbContext<AppDbContext>(options => options.UseNpgsql(Configuration.GetConnectionString("connectionString"), b => b.MigrationsAssembly("SistemuPagrindai")));
            services.AddIdentity<AspNetUsers, IdentityRole>(e =>
            {
                e.Password.RequireUppercase = false;
                e.Password.RequiredUniqueChars = 0;
                e.Password.RequiredLength = 6;
                e.Password.RequireNonAlphanumeric = false;

                e.User.RequireUniqueEmail = true;

                e.SignIn.RequireConfirmedEmail = true;
                e.SignIn.RequireConfirmedAccount = true;
            })
                //.AddRoles<IdentityRole>()
                .AddEntityFrameworkStores<AppDbContext>()
                .AddDefaultUI()
                .AddDefaultTokenProviders();
            services.AddControllersWithViews();
            services.AddRazorPages();
            services.AddServerSideBlazor();
            services.AddScoped<AuthenticationStateProvider, ServerAuthenticationStateProvider>();
            services.AddSingleton<HttpClient>();
            services.AddSingleton<ExampleService>();
            services.AddSingleton<CartItemService>();

            var emailConfig = Configuration
                .GetSection("EmailConfiguration")
                .Get<EmailConfiguration>();
            services.AddSingleton(emailConfig);
            services.AddScoped<IEmailSender, EmailSender>();
            services.AddSingleton<ItemService>();
            services.AddSingleton<ItemRouteService>();
            services.AddSingleton<RoutesService>();
            services.AddSingleton<CartService>();
            services.AddBlazoredModal();
            services.AddBlazoredToast();
            services.AddAutoMapper(typeof(Startup));
            services.AddMailKit(Configure => Configuration.GetSection("email").Get<MailKitOptions>());
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env, IServiceProvider services)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapControllers();
                endpoints.MapBlazorHub();
                endpoints.MapFallbackToPage("/_Host");
            });

            

            CreateUserRoles(services).GetAwaiter().GetResult();
        }

        public async Task CreateUserRoles(IServiceProvider serviceProdiver)
        {
            var RoleManager = serviceProdiver.GetRequiredService<RoleManager<IdentityRole>>();
            var UserManager = serviceProdiver.GetRequiredService<UserManager<AspNetUsers>>();

            IdentityResult roleResult;

            var roleCheck = await RoleManager.RoleExistsAsync("Admin");
            if (!roleCheck)
            {
                roleResult = await RoleManager.CreateAsync(new IdentityRole("Admin"));
            }

            var workerRole = await RoleManager.RoleExistsAsync("Worker");
            if (!workerRole)
            {
                roleResult = await RoleManager.CreateAsync(new IdentityRole("Worker"));
            }

            var clientRole = await RoleManager.RoleExistsAsync("Client");
            if (!clientRole)
            {
                roleResult = await RoleManager.CreateAsync(new IdentityRole("Client"));
            }

            // Users who have admin role
            AspNetUsers user = await UserManager.FindByNameAsync("magahakis@gmail.com");
            AspNetUsers worker = await UserManager.FindByNameAsync("martynas.cepas1@gmail.com");
            AspNetUsers client = await UserManager.FindByNameAsync("tubutis10@gmail.com");
            if (user != null)
            {
                await UserManager.AddToRoleAsync(user, "Admin");
            }
            if (worker != null)
            {
                await UserManager.AddToRoleAsync(worker, "Worker");
            }
            if (client != null)
            {
                await UserManager.AddToRoleAsync(client, "Client");
            }
            // Users who have worker role

            var User = new AspNetUsers();
        }
    }
}
