﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SistemuPagrindai.Helpers;
using SistemuPagrindai.Mappings.ItemRoutes;

namespace SistemuPagrindai.Services
{
    public class ItemRouteService
    {
        private readonly HttpClient client;
        private string baseUrl;

        public ItemRouteService(HttpClient client)
        {
            this.client = client;
            baseUrl = "https://localhost:44365";
        }

        public async Task<ItemRouteListViewModel> GetItemRoutesAsync()
        {
            var json = await client.GetStringAsync($"{baseUrl}/api/ItemRoute/");
            return JsonConvert.DeserializeObject<ItemRouteListViewModel>(json);
        }

        public async Task<int> InsertItemAsync(CreateItemRouteCommand command)
        {
            var result =  await client.PostAsync($"{baseUrl}/api/ItemRoute/", RequestHelper.GetStringContentFromObject(command));
            return Convert.ToInt32(result.Content.ReadAsStringAsync().Result);
        }
    }
}
