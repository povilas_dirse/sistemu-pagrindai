﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SistemuPagrindai.Helpers;
using SistemuPagrindai.Mappings.Items;

namespace SistemuPagrindai.Services
{
    public class ItemService
    {
        private readonly HttpClient client;
        private string baseUrl;

        public ItemService(HttpClient client)
        {
            this.client = client;
            baseUrl = "https://localhost:44365";
        }

        public async Task<ItemListViewModel> GetItemsAsync()
        {
            var json = await client.GetStringAsync($"{baseUrl}/api/Items/");
            return JsonConvert.DeserializeObject<ItemListViewModel>(json);
        }

        public async Task<ItemViewModel> GetItemAsync(int id)
        {
            try
            {
                var json = await client.GetStringAsync($"{baseUrl}/api/Items/{id}");
                return JsonConvert.DeserializeObject<ItemViewModel>(json);
            }
            catch(Exception e)
            {
                return null;
            }
        }

        public async Task<int> InsertItemAsync(CreateItemCommand command)
        {
            var result =  await client.PostAsync($"{baseUrl}/api/Items/", RequestHelper.GetStringContentFromObject(command));
            return Convert.ToInt32(result.Content.ReadAsStringAsync().Result);
        }

        public async Task<int> UpdateItemAsync(UpdateItemCommand command)
        {
           var result = await client.PutAsync($"{baseUrl}/api/Items/", RequestHelper.GetStringContentFromObject(command));
           return Convert.ToInt32(result.Content.ReadAsStringAsync().Result);
        }

        public async Task<HttpResponseMessage> DeleteItemAsync(int id)
        {
            return await client.DeleteAsync($"{baseUrl}/api/Items/{id}");
        }
    }
}
