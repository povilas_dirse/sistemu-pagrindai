﻿using Newtonsoft.Json;
using SistemuPagrindai.Mappings.Carts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SistemuPagrindai.Services
{
    public class CartService
    {
        private readonly HttpClient client;
        private string baseUrl;

        public CartService(HttpClient client)
        {
            this.client = client;
            baseUrl = "https://localhost:44365";
        }

        public async Task<CartViewModel> GetUserCartAsync(string userId)
        {
            try
            {
                var json = await client.GetStringAsync($"{baseUrl}/api/Carts/{userId}");
                return JsonConvert.DeserializeObject<CartViewModel>(json);
            }
            catch (Exception e)
            {

                throw;
            }
            
        }
    }
}
