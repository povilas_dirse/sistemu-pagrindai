﻿using Domain.Models;
using Newtonsoft.Json;
using SistemuPagrindai.Helpers;
using SistemuPagrindai.Mappings.Orders;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;

namespace SistemuPagrindai.Services
{
    public class ExampleService
    {
        private readonly HttpClient client;
        private string baseUrl;

        public ExampleService(HttpClient client)
        {
            this.client = client;
            baseUrl = "https://localhost:44365";
        }

        public async Task<OrderListViewModel> GetOrdersAsync(string userId)
        {
            var json = await client.GetStringAsync($"{baseUrl}/api/Orders?userId={userId}");
            return JsonConvert.DeserializeObject<OrderListViewModel>(json);
        }

        public async Task<OrderViewModel> GetOrderAsync(int id)
        {
            try
            {
                var json = await client.GetStringAsync($"{baseUrl}/api/Orders/{id}");
                return JsonConvert.DeserializeObject<OrderViewModel>(json);
            }
            catch (Exception e)
            {
                return null;
            }
        }

        public async Task<int> InsertOrderAsync(CreateOrderCommand command)
        {
            var result = await client.PostAsync($"{baseUrl}/api/Orders/", RequestHelper.GetStringContentFromObject(command));
            return Convert.ToInt32(result.Content.ReadAsStringAsync().Result);
        }

        public async Task<int> UpdateOrderAsync(UpdateOrderCommand command)
        {
            var result = await client.PutAsync($"{baseUrl}/api/Orders/", RequestHelper.GetStringContentFromObject(command));
            return Convert.ToInt32(result.Content.ReadAsStringAsync().Result);
        }

        public async Task<HttpResponseMessage> DeleteOrderAsync(int id)
        {
            return await client.DeleteAsync($"{baseUrl}/api/Orders/{id}");
        }
    }
}
