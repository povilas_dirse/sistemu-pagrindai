﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SistemuPagrindai.Helpers;
using SistemuPagrindai.Mappings.CartItem;

namespace SistemuPagrindai.Services
{
    public class CartItemService
    {
        private readonly HttpClient client;
        private string baseUrl;

        public CartItemService(HttpClient client)
        {
            this.client = client;
            baseUrl = "https://localhost:44365";
        }

        public async Task<CartItemListViewModel> GetCartItemsAsync(int? cartId)
        {
            var json = await client.GetStringAsync($"{baseUrl}/api/CartItem?cartId={cartId}");
            return JsonConvert.DeserializeObject<CartItemListViewModel>(json);
        }

        public async Task<int> InsertCartItemAsync(CreateCartItemCommand command)
        {
            var result = await client.PostAsync($"{baseUrl}/api/CartItem/", RequestHelper.GetStringContentFromObject(command));
            return Convert.ToInt32(result.Content.ReadAsStringAsync().Result);
        }

        public async Task<HttpResponseMessage> DeleteCartItem(int id)
        {
            return await client.DeleteAsync($"{baseUrl}/api/CartItem/{id}");
        }
    }
}
