﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using Newtonsoft.Json;
using SistemuPagrindai.Mappings.Routes;

namespace SistemuPagrindai.Services
{
    public class RoutesService
    {
        private readonly HttpClient client;
        private string baseUrl;

        public RoutesService(HttpClient client)
        {
            this.client = client;
            baseUrl = "https://localhost:44365";
        }

        public async Task<RoutesListViewModel> GetRoutesAsync()
        {
            var json = await client.GetStringAsync($"{baseUrl}/api/Routes/");
            return JsonConvert.DeserializeObject<RoutesListViewModel>(json);
        }
    }
}
