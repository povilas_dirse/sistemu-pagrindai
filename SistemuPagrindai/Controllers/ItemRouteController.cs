﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Models;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SistemuPagrindai.Data;
using SistemuPagrindai.Mappings.ItemRoutes;

namespace SistemuPagrindai.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemRouteController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;

        public ItemRouteController(AppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ItemRouteListViewModel> GetAll()
        {
            try
            {
                var model = new ItemRouteListViewModel()
                {
                    RouteItems = _mapper.Map<List<ItemRouteViewModel>>(_context.ItemRoutes.Include(e => e.Item).Include(e => e.Route))
                };
                return model;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        [HttpPost]
        public async Task<ActionResult<int>> Post([FromBody] CreateItemRouteCommand command)
        {
            var entity = new ItemRoute
            {
                ItemId = command.ItemId,
                Item = command.item,
                Route = command.route,
                RouteDate = command.RouteDate,
                RouteId = command.RouteId
            };

            _context.ItemRoutes.Add(entity);

            await _context.SaveChangesAsync();
            return Ok(entity.Id);
        }
    }
}
