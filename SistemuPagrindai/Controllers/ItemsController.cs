﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SistemuPagrindai.Data;
using SistemuPagrindai.Mappings.Items;
using SistemuPagrindai.Migrations;

namespace SistemuPagrindai.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ItemsController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;

        public ItemsController(AppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<ItemListViewModel> GetAll()
        {
            var model = new ItemListViewModel()
            {
                Items = _mapper.Map<List<ItemViewModel>>(_context.Item)
            };

            return model;
        }

        [HttpGet("{id}")]
        public async Task<ItemViewModel> GetItem(int id)
        {
            try
            {
                var dto = _mapper.Map<ItemViewModel>(await _context.Item.FirstOrDefaultAsync(e => e.Id == id));

                return dto;
            }
            catch (Exception e)
            {
                return null;
            }
        }

        [HttpPost]
        public async Task<ActionResult<int>> Post([FromBody] CreateItemCommand command)
        {
            var entity = new Item
            {
                Name = command.Name,
                Count = command.Count,
                Description = command.Description,
                ExpiryDate = command.ExpiryDate,
                Location = command.Location,
                Manufacturer = command.Manufacturer,
                Price = command.Price,
                Rating = command.Rating
            };

            _context.Item.Add(entity);

            await _context.SaveChangesAsync();
            return Ok(entity.Id);
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Put([FromBody] UpdateItemCommand command)
        {
            var entity = await _context.Item.FirstOrDefaultAsync(e => e.Id == command.Id);

            entity.Name = command.Name;
            entity.Count = command.Count;
            entity.Description = command.Description;
            entity.ExpiryDate = command.ExpiryDate;
            entity.Location = command.Location;
            entity.Manufacturer = command.Manufacturer;
            entity.Price = command.Price;
            entity.Rating = command.Rating;

            await _context.SaveChangesAsync();

            return Ok(entity.Id);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _context.Item.FirstOrDefaultAsync(e => e.Id == id);

            if (result != null)
            {
                _context.Remove(result);
                await _context.SaveChangesAsync();
            }

            return Ok();
        }
    }
}
