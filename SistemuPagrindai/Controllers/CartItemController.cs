﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SistemuPagrindai.Data;
using SistemuPagrindai.Mappings.CartItem;

namespace SistemuPagrindai.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartItemController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;

        public CartItemController(AppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<CartItemListViewModel> GetAll(int? cartId)
        {
            try
            {
                var model = new CartItemListViewModel()
                {
                    CartItems = _mapper.Map<List<CartItemViewModel>>(_context.CartItem.Include(u => u.Item).Where(e => e.CartId == cartId))
                };
                return model;
            }
            catch (Exception e)
            {
                return null;
            }

        }

        [HttpPost]
        public async Task<ActionResult<int>> Post([FromBody] CreateCartItemCommand command)
        {
            var item = await _context.Item.FirstOrDefaultAsync(e => e.Id == command.ItemId).ConfigureAwait(false);

            if (item == null)
            {
                return Ok(0);
            }

            var cart = await _context.Cart.FirstOrDefaultAsync(e => e.UserId == command.UserId && e.Status == 0).ConfigureAwait(false);

            if (cart == null)
            {
                try
                {
                    var newCart = new Cart
                    {
                        UserId = command.UserId,
                        price = 0,
                        createdAt = DateTime.Now,
                        Status = 0,
                    };

                    _context.Cart.Add(newCart);
                    await _context.SaveChangesAsync();
                }
                catch (Exception e)
                {

                    throw;
                }
                

                cart = await _context.Cart.FirstOrDefaultAsync(e => e.UserId == command.UserId).ConfigureAwait(false);
            }

            if (item.Count > command.Quantity)
            {
                cart.price = cart.price + (item.Price * command.Quantity);
                item.Count = item.Count - command.Quantity;

                await _context.SaveChangesAsync();
            }
            else
            {
                cart.price = cart.price + (item.Price * command.Quantity);
                item.Count = 0;

                await _context.SaveChangesAsync();
            }

            var entity = new CartItem
            {
                ItemId = item.Id,
                CartId = cart.Id,
                Quantity = command.Quantity
            };

            _context.CartItem.Add(entity);

            await _context.SaveChangesAsync();
            return Ok(entity.Id);
        }

        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _context.CartItem.FirstOrDefaultAsync(e => e.Id == id);

            if (result != null)
            {
                _context.Remove(result);
                await _context.SaveChangesAsync();
            }

            return Ok();
        }
    }
}
