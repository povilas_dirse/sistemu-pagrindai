﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using Domain.Models;
using SistemuPagrindai.Data;
using AutoMapper;
using SistemuPagrindai.Mappings.Orders;
using Microsoft.AspNetCore.Http;
using SistemuPagrindai.Helpers;

namespace SistemuPagrindai.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrdersController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;

        public OrdersController(AppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<OrderListViewModel> GetAll(string userId)
        {
            if (string.IsNullOrEmpty(userId))
            {
                var model = new OrderListViewModel
                {
                    Orders = _mapper.Map<List<OrderViewModel>>(_context.Orders)
                };

                return model;
            }
            else
            {
                var model = new OrderListViewModel
                {
                    Orders = _mapper.Map<List<OrderViewModel>>(_context.Orders.Where(e => e.UserId == userId))
                };

                return model;
            }
        }

        [HttpGet("{id}")]
        public async Task<OrderViewModel> GetOrder(int id)
        {
            var dto = _mapper.Map<OrderViewModel>(await _context.Orders.FirstOrDefaultAsync(e => e.Id == id));

            return dto;
        }

        [HttpPost]
        public async Task<ActionResult<int>> Post([FromBody] CreateOrderCommand command)
        {
            var entity = new Order
            {
                OrderDate = DateTime.Now,
                Price = command.Quantity,
                Status = command.Status,
                Adress = command.Adress,
                DeliveyType = command.DeliveyType,
                Quantity = 0,
                Discount = 0,
                CartId = command.CartId,
                UserId = command.UserId,
            };

            _context.Orders.Add(entity);

            var cart = await _context.Cart.FirstOrDefaultAsync(e => e.Id == command.CartId).ConfigureAwait(false);

            cart.Status = 1;

            await _context.SaveChangesAsync();

            return Ok(entity.Id);
        }

        [HttpPut]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Put([FromBody] UpdateOrderCommand command)
        {
            var entity = await _context.Orders.FirstOrDefaultAsync(e => e.Id == command.Id);

            if (entity == null)
            {
                throw new NotFoundException(nameof(Order), command.Id);
            }

                entity.Id = command.Id;
                entity.OrderDate = command.OrderDate;
                entity.Price = command.Quantity;
                entity.Status = command.Status;
                entity.Adress = command.Adress;
                entity.DeliveyType = command.DeliveyType;
                entity.Quantity = command.Quantity;
                entity.Discount = command.Discount;

            await _context.SaveChangesAsync();

            return NoContent();
        }
        [HttpDelete("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Delete(int id)
        {
            var result = await _context.Orders.FirstOrDefaultAsync(e => e.Id == id);

            if (result != null)
            {
                _context.Remove(result);
                await _context.SaveChangesAsync();
            }

            return Ok();
        }
    }

}
