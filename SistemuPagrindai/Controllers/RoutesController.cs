﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using SistemuPagrindai.Data;
using SistemuPagrindai.Mappings.Routes;

namespace SistemuPagrindai.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class RoutesController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;

        public RoutesController(AppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<RoutesListViewModel> GetAllRoutes()
        {
            try
            {
                var model = new RoutesListViewModel()
                {
                    Routes = _mapper.Map<List<RoutesViewModel>>(_context.Route)
                };

                return model;

            }
            catch (Exception e)
            {
                return null;
            }
        }
    }
}
