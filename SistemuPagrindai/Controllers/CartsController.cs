﻿using AutoMapper;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SistemuPagrindai.Data;
using SistemuPagrindai.Mappings.Carts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemuPagrindai.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CartsController : ControllerBase
    {
        private readonly AppDbContext _context;
        private readonly IMapper _mapper;

        public CartsController(AppDbContext context, IMapper mapper)
        {
            _context = context;
            _mapper = mapper;
        }

        [HttpGet]
        public async Task<CartsListViewModel> GetAll(string userId)
        {
            try
            {
                var model = new CartsListViewModel
                {
                    Carts = _mapper.Map<List<CartViewModel>>(_context.Cart.Where(e => e.UserId == userId && e.Status == 0))
                };

                return model;
            }
            catch (Exception e)
            {

                throw;
            }
            
        }

        [HttpGet("{userId}")]
        public async Task<CartViewModel> Get(string userId)
        {
            try
            {
                var dto = _mapper.Map<CartViewModel>(await _context.Cart.FirstOrDefaultAsync(e => e.UserId == userId && e.Status == 0));

                return dto;
            }
            catch (Exception)
            {

                throw;
            }
            
        }
    }
}
