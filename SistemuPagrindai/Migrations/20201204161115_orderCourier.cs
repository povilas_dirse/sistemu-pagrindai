﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemuPagrindai.Migrations
{
    public partial class orderCourier : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "OrderCourier",
                table: "Orders",
                type: "text",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "OrderCourier",
                table: "Orders");
        }
    }
}
