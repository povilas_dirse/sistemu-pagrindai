﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

namespace SistemuPagrindai.Migrations
{
    public partial class ItemRoute : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RouteDate",
                table: "Route");

            migrationBuilder.AddColumn<DateTime>(
                name: "RouteDate",
                table: "ItemRoutes",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "RouteDate",
                table: "ItemRoutes");

            migrationBuilder.AddColumn<DateTime>(
                name: "RouteDate",
                table: "Route",
                type: "timestamp without time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }
    }
}
