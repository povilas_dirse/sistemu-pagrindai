﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemuPagrindai.Mappings.Orders
{
    public class OrderListViewModel
    {
        public List<OrderViewModel> Orders { get; set; }
    }
}
