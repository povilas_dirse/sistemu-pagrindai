﻿using AutoMapper;

namespace SistemuPagrindai.Mappings.Orders
{
    public class OrderProfile : Profile
    {
        public OrderProfile()
        {
            CreateMap<Domain.Models.Order, OrderViewModel>()
                .ForMember(pDTO => pDTO.Id, opt => opt.MapFrom(e => e.Id))
                .ForMember(pDTO => pDTO.OrderDate, opt => opt.MapFrom(e => e.OrderDate))
                .ForMember(pDTO => pDTO.Price, opt => opt.MapFrom(e => e.Price))
                .ForMember(pDTO => pDTO.Status, opt => opt.MapFrom(e => e.Status))
                .ForMember(pDTO => pDTO.Adress, opt => opt.MapFrom(e => e.Adress))
                .ForMember(pDTO => pDTO.DeliveyType, opt => opt.MapFrom(e => e.DeliveyType)) 
                .ForMember(pDTO => pDTO.Quantity, opt => opt.MapFrom(e => e.Quantity))
                .ForMember(pDTO => pDTO.Discount, opt => opt.MapFrom(e => e.Discount))
                .ForMember(pDTO => pDTO.CartId, opt => opt.MapFrom(e => e.CartId));
        }
    }
}
