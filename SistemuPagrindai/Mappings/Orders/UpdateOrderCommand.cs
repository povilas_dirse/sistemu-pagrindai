﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemuPagrindai.Mappings.Orders
{
    public class UpdateOrderCommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string OrderCourier { get; set; }
        public DateTime? OrderDate { get; set; }
        public double Price { get; set; }
        public string Status { get; set; }
        public string Adress { get; set; }
        public string DeliveyType { get; set; }
        public int Quantity { get; set; }
        public double Discount { get; set; }
    }
}
