﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemuPagrindai.Mappings.Items
{
    public class UpdateItemCommand
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string Manufacturer { get; set; }
        public int Count { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public string Rating { get; set; }
        public string ExpiryDate { get; set; }
    }
}
