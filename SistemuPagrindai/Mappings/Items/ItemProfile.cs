﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using SistemuPagrindai.Mappings.ItemRoutes;

namespace SistemuPagrindai.Mappings.Items
{
    public class ItemProfile : Profile
    {
        public ItemProfile()
        {
            CreateMap<Domain.Models.Item, ItemViewModel>()
                .ForMember(pDTO => pDTO.Id, opt => opt.MapFrom(e => e.Id))
                .ForMember(pDTO => pDTO.Count, opt => opt.MapFrom(e => e.Count))
                .ForMember(pDTO => pDTO.Description, opt => opt.MapFrom(e => e.Description))
                .ForMember(pDTO => pDTO.ExpiryDate, opt => opt.MapFrom(e => e.ExpiryDate))
                .ForMember(pDTO => pDTO.Location, opt => opt.MapFrom(e => e.Location))
                .ForMember(pDTO => pDTO.Manufacturer, opt => opt.MapFrom(e => e.Manufacturer))
                .ForMember(pDTO => pDTO.Name, opt => opt.MapFrom(e => e.Name))
                .ForMember(pDTO => pDTO.Price, opt => opt.MapFrom(e => e.Price))
                .ForMember(pDTO => pDTO.Rating, opt => opt.MapFrom(e => e.Rating))
                .ForMember(pDTO => pDTO.ItemRoutes, opt => opt.MapFrom(e => e.ItemRoutes.Any() ?
                                                                                    e.ItemRoutes.Select(u => new ItemRouteViewModel
                                                                                    {
                                                                                        ItemName = u.Item.Name,
                                                                                        ItemId = u.ItemId,
                                                                                        RouteCode = u.Route.Code,
                                                                                        RouteId = u.RouteId,
                                                                                        Id = u.Id,
                                                                                        RouteDate = u.RouteDate
                                                                                    })  :null));
        }
    }
}
