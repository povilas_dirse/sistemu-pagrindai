﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemuPagrindai.Mappings.Items
{
    public class ItemListViewModel
    {
        public List<ItemViewModel> Items { get; set; }
    }
}
