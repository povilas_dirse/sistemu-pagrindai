﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;
using SistemuPagrindai.Mappings.ItemRoutes;

namespace SistemuPagrindai.Mappings.Items
{
    public class ItemViewModel
    {
        public int Id { get; set; }
        [Required]
        [StringLength(50, ErrorMessage = "Item name has to be shorter than 50 words")]
        public string Name { get; set; }
        [Required]
        [Range(0.1, 10000, ErrorMessage = "Price has to be in range from 0.1 to 10000")]
        public double Price { get; set; }
        [Required]
        public string Manufacturer { get; set; }
        [Required]
        [Range(1, 10000, ErrorMessage = "Count has to be in range from 1 to 10000")]
        public int Count { get; set; }
        [Required]
        [StringLength(200, ErrorMessage = "Item location has to be shorter than 200 words")]
        public string Location { get; set; }
        public string Description { get; set; }
        public string Rating { get; set; }
        public string ExpiryDate { get; set; }
        public IEnumerable<ItemRouteViewModel> ItemRoutes { get; set; }
    }
}
