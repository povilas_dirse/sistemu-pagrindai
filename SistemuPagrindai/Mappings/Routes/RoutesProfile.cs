﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;

namespace SistemuPagrindai.Mappings.Routes
{
    public class RouteProfile : Profile
    {
        public RouteProfile()
        {
            CreateMap<Domain.Models.Route, RoutesViewModel>()
                .ForMember(pDTO => pDTO.Id, opt => opt.MapFrom(e => e.Id))
                .ForMember(pDTO => pDTO.Code, opt => opt.MapFrom(e => e.Code))
                .ForMember(pDTO => pDTO.From, opt => opt.MapFrom(e => e.From))
                .ForMember(pDTO => pDTO.To, opt => opt.MapFrom(e => e.To));
        }
    }
}
