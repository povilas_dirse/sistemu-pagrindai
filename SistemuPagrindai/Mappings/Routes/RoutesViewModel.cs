﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemuPagrindai.Mappings.Routes
{
    public class RoutesViewModel
    {
        public int Id { get; set; }
        public string Code { get; set; }
        public string From { get; set; }
        public string To { get; set; }
    }
}
