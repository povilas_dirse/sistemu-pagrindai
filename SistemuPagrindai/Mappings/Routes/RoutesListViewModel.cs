﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemuPagrindai.Mappings.Routes
{
    public class RoutesListViewModel
    {
        public List<RoutesViewModel> Routes { get; set; }
    }
}
