﻿using SistemuPagrindai.Mappings.Items;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemuPagrindai.Mappings.Carts
{
    public class CartViewModel
    {
        public int Id { get; set; }
        public double Price { get; set; }
        public int Status { get; set; }
        public string UserId { get; set; }
        public DateTime CreatedAt { get; set; }
        public IEnumerable<ItemViewModel> Items { get; set; }
    }
}
