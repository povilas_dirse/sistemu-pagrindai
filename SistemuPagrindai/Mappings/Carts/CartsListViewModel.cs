﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemuPagrindai.Mappings.Carts
{
    public class CartsListViewModel
    {
        public List<CartViewModel> Carts { get; set; }
    }
}
