﻿using AutoMapper;
using Domain.Models;
using SistemuPagrindai.Mappings.CartItem;
using System.Linq;

namespace SistemuPagrindai.Mappings.Carts
{
    public class CartProfile : Profile
    {
        public CartProfile()
        {
            CreateMap<Cart, CartViewModel>()
                .ForMember(pDTO => pDTO.Id, opt => opt.MapFrom(e => e.Id))
                .ForMember(pDTO => pDTO.UserId, opt => opt.MapFrom(e => e.UserId))
                .ForMember(pDTO => pDTO.Price, opt => opt.MapFrom(e => e.price))
                .ForMember(pDTO => pDTO.Status, opt => opt.MapFrom(e => e.Status))
                .ForMember(pDTO => pDTO.CreatedAt, opt => opt.MapFrom(e => e.createdAt))
                .ForMember(pDTO => pDTO.Items, opt => opt.MapFrom(e => e.CartItems.Any() ?
                                                                            e.CartItems.Select(u => new CartItemViewModel
                                                                            {
                                                                                ItemId = u.Item.Id,
                                                                                CartId = u.Cart.Id,
                                                                                Quantity = u.Quantity,
                                                                                ItemName = u.Item.Name
                                                                            }) : null));
        }
    }
}
