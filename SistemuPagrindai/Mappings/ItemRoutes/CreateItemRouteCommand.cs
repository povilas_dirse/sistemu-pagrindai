﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Models;

namespace SistemuPagrindai.Mappings.ItemRoutes
{
    public class CreateItemRouteCommand
    {
        public int ItemId { get; set; }
        public Item item { get; set; }

        public Route route { get; set; }
        public int RouteId { get; set; }

        public DateTime RouteDate { get; set; }
    }
}
