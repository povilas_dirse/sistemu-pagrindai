﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemuPagrindai.Mappings.ItemRoutes
{
    public class ItemRouteListViewModel
    {
        public List<ItemRouteViewModel> RouteItems { get; set; }
    }
}
