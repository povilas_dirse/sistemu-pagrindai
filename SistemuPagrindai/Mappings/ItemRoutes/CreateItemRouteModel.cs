﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemuPagrindai.Mappings.ItemRoutes
{
    public class CreateItemRouteModel
    {
        [Required]
        public string ItemId { get; set; }
        [Required]
        public string RouteId { get; set; }
        [Required]
        public DateTime RouteDate { get; set; }
    }
}
