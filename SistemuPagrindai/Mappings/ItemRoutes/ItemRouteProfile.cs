﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Models;

namespace SistemuPagrindai.Mappings.ItemRoutes
{
    public class ItemRouteProfile : Profile
    {
        public ItemRouteProfile()
        {
            CreateMap<ItemRoute, ItemRouteViewModel>()
                .ForMember(pDTO => pDTO.Id, opt => opt.MapFrom(e => e.Id))
                .ForMember(pDTO => pDTO.RouteId, opt => opt.MapFrom(e => e.RouteId))
                .ForMember(pDTO => pDTO.ItemId, opt => opt.MapFrom(e => e.ItemId))
                .ForMember(pDTO => pDTO.ItemName, opt => opt.MapFrom(e => e.Item != null ? e.Item.Name : null))
                .ForMember(pDTO => pDTO.RouteCode, opt => opt.MapFrom(e => e.Route != null ? e.Route.Code : null))
                .ForMember(pDTO => pDTO.RouteDate, opt => opt.MapFrom(e => e.RouteDate));
        }
    }
}
