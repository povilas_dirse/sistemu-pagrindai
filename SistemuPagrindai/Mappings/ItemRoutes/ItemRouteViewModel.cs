﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace SistemuPagrindai.Mappings.ItemRoutes
{
    public class ItemRouteViewModel
    {
        public int Id { get; set; }

        [Required]
        public int ItemId { get; set; }
        public string ItemName { get; set; }

        public string RouteCode { get; set; }

        [Required]
        public int RouteId { get; set; }
        [Required]
        public DateTime RouteDate { get; set; }
    }
}
