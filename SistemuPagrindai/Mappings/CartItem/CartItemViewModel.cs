﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemuPagrindai.Mappings.CartItem
{
    public class CartItemViewModel
    {
        public int Id { get; set; }
        public int ItemId { get; set; }
        public int CartId { get; set; }
        public int Quantity { get; set; }
        public string ItemName { get; set; }
        public double Price { get; set; }
    }
}
