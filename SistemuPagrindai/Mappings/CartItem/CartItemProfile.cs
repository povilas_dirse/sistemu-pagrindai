﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AutoMapper;
using Domain.Models;

namespace SistemuPagrindai.Mappings.CartItem
{
    public class CartItemProfile : Profile
    {
        public CartItemProfile()
        {
            CreateMap<Domain.Models.CartItem, CartItemViewModel>()
                .ForMember(pDTO => pDTO.Id, opt => opt.MapFrom(e => e.Id))
                .ForMember(pDTO => pDTO.CartId, opt => opt.MapFrom(e => e.CartId))
                .ForMember(pDTO => pDTO.ItemId, opt => opt.MapFrom(e => e.ItemId))                
                .ForMember(pDTO => pDTO.Quantity, opt => opt.MapFrom(e => e.Quantity))
                .ForMember(pDTO => pDTO.ItemName, opt => opt.MapFrom(e => e.Item.Name))
                .ForMember(pDTO => pDTO.Price, opt => opt.MapFrom(e => e.Item.Price))
                .ForMember(pDTO => pDTO.CartId, opt => opt.MapFrom(e => e.CartId));
        }
    }
}
