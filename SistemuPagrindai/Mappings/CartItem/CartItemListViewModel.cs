﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace SistemuPagrindai.Mappings.CartItem
{
    public class CartItemListViewModel
    {
        public List<CartItemViewModel> CartItems { get; set; }
    }
}
