﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Domain.Models;

namespace SistemuPagrindai.Mappings.CartItem
{
    public class CreateCartItemCommand
    {
        public int ItemId { get; set; }
        public int CartId { get; set; }
        public int Quantity { get; set; }
        public string UserId { get; set; }
    }
}
