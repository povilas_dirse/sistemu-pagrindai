﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
    public class Cart
    {
        public Cart()
        {
            CartItems = new List<CartItem>();
        }
        public List<CartItem> CartItems { get; set; }
        public int Id { get; set; }
        public double price { get; set; }
        public DateTime? createdAt { get; set; }
        public Order Order { get; set; }
        public string UserId { get; set; }
        public int Status { get; set; }
    }
}
