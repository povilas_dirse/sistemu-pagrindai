﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
    public class Item
    {
        public Item()
        {
            ItemRoutes = new List<ItemRoute>();
            CartItems = new List<CartItem>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }
        public string Manufacturer { get; set; }
        public int Count { get; set; }
        public string Location { get; set; }
        public string Description { get; set; }
        public string Rating { get; set; }
        public string ExpiryDate { get; set; }

        public List<ItemRoute> ItemRoutes { get; set; }
        public List<CartItem> CartItems { get; set; }
    }
}
