﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
    public class Route
    {
        public Route()
        {
            ItemRoutes = new List<ItemRoute>();
        }
        public int Id { get; set; }
        public string Code { get; set; }
        public string From { get; set; }
        public string To { get; set; }
        public List<ItemRoute> ItemRoutes { get; set; }
    }
}
