﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Text;

namespace Domain.Models
{
    public class Order
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string OrderCourier { get; set; }
        public DateTime? OrderDate { get; set; }
        public double Price { get; set; }
        public string Status { get; set; }
        public string Adress { get; set; }
        public string DeliveyType { get; set; }
        public int Quantity { get; set; }
        public double Discount { get; set; }
        public int? CartId { get; set; }
        public Cart Cart { get; set; }
        public string UserId { get; set; }
    }
}
