﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
    public class ItemRoute
    { 
        public int Id { get; set; }

        public int ItemId { get; set; }
        public Item Item { get; set; }

        public Route Route { get; set; }
        public int RouteId { get; set; }

        public DateTime RouteDate { get; set; }
    }
}
