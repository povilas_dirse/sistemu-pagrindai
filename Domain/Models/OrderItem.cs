﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Domain.Models
{
    public class OrderItem
    {
        public int Id { get; set; }
        public int Quantity { get; set; }
    }
}
